# Personal Project Phase 2

**Ben Puhalski** - CS 3415

# New Features

- Adds Mandatory client id argument to ClientConsole and ChatClient
- Saves ID to ConnectionToClient
- Displays ID to server and back to clients

# Dependencies 

Uses the "OCSF" and "SimpleChat phase 1" from http://www.site.uottawa.ca/school/research/lloseng/.

Also uses, personalPhaseOne from my personal project phase 1.

```java
package com.lloseng.ocsf.client;
package com.lloseng.ocsf.client;
package common;
package client;
package server;
```

# Code

```java
  public static void main(String[] args) throws Exception {
	.............
   	.............
    .............
    //Host client on inputted port number
    ClientConsole chat= new ClientConsole(id, host, port);
    chat.client.sendToServer("#login " + id);
    chat.accept();  //Wait for console data
  }
}
```

New ClientConsole main() uses id to create ClientConsole as an argument as well as sends the id using the #login command to the server to be handled in EchoServer.

```java
 public void handleMessageFromClient
    (Object msg, ConnectionToClient client)
  {
    System.out.println("Message received: " + msg + " from " + client.getInfo("id") + " " + client);

    //make sure to test if msg is a string before checking the substring of it
    if(msg instanceof String) {
      if (((String) msg).startsWith("#login") && client.getInfo("id") == null) {
        //detected login command
        if( ((String) msg).length() > 7 ) {
          // Set id to the client info
          client.setInfo("id", ((String) msg).substring(7)); //client.getInfo("id") = the login id
          System.out.println("Setting " + client + " to have id " + client.getInfo("id"));
        } else {
          System.out.println("error no id");
        }
      } else if(((String) msg).startsWith("#login") && client.getInfo("id") != null) {
        //detect if #login command is used before id is set for the client
        try {
          client.sendToClient("Error \'#login\' detected!");
        } catch (IOException e) {
          e.printStackTrace();
        }
      } else if(client.getInfo("id") == null){
        //#login command not received as the first command
        try {
          client.sendToClient("Error, no id set");
          client.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      } else {
        this.sendToAllClients(client.getInfo("id") + ": " + msg);
      }
    } else {
      this.sendToAllClients(client.getInfo("id") + ": " + msg);
    }
  }
```

Many if and else statements are used but that is because there are many specific cases outlined in the assignment to be handled by the server. Essentially: 

1. The server handles a **(#login 'id')**  command as the **first** message from a new client
2. Then handles any other messages (which does not include #login **at the start** of the message) and sends the id appended with the message after back to all clients.

Any other variation should be handled by the above code block.
