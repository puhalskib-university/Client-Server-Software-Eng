// This file contains material supporting section 3.7 of the textbook:
// "Object Oriented Software Engineering" and is issued under the open-source
// license found at www.lloseng.com 

import java.io.*;
import client.*;
import common.*;

/**
 * This class constructs the UI for a chat client.  It implements the
 * chat interface in order to activate the display() method.
 * Warning: Some of the code here is cloned in ServerConsole 
 *
 * @author Fran&ccedil;ois B&eacute;langer
 * @author Dr Timothy C. Lethbridge  
 * @author Dr Robert Lagani&egrave;re
 * @version July 2000
 */
public class ClientConsole implements ChatIF 
{
  //Class variables *************************************************
  
  /**
   * The default port to connect on.
   */
  final public static int DEFAULT_PORT = 5555;
  
  //Instance variables **********************************************
  
  /**
   * The instance of the client that created this ConsoleChat.
   */
  ChatClient client;

  
  //Constructors ****************************************************

  /**
   * Constructs an instance of the ClientConsole UI.
   *
   * @param host The host to connect to.
   * @param port The port to connect on.
   */
  public ClientConsole(String ID, String host, int port)
  {
    try 
    {
      client= new ChatClient(ID, host, port, this);
    } 
    catch(IOException exception) 
    {
      System.out.println("Error: Can't setup connection!"
                + " Terminating client.");
      System.exit(1);
    }
  }

  
  //Instance methods ************************************************
  
  /**
   * This method waits for input from the console.  Once it is 
   * received, it sends it to the client's message handler.
   */
  public void accept() 
  {
    try
    {
      BufferedReader fromConsole = 
        new BufferedReader(new InputStreamReader(System.in));
      String message;

      while (true) 
      {
        message = fromConsole.readLine();
        //handle client commands
        if(!messageCommand(message)) {
          client.handleMessageFromClientUI(message);
        }
      }
    } 
    catch (Exception ex) 
    {
      System.out.println
        ("Unexpected error while reading from console!");
    }
  }

  /**
   * This method overrides the method in the ChatIF interface.  It
   * displays a message onto the screen.
   *
   * @param message The string to be displayed.
   */
  public void display(String message) 
  {
    System.out.println("> " + message);
  }

  //Private methods ***************************************************
  private boolean messageCommand(String message) {
    if(message.contains("#")) {
      if(message.equalsIgnoreCase("#quit")) {
        //terminate connection before exiting program
        client.quit();
      } else if(message.equalsIgnoreCase("#logoff")) {
        try {
          client.closeConnection();
        } catch (IOException ex) {
          System.out.println("IOException: could not close connection between client and server. Quitting...");
          client.quit();
        }
      } else if(message.equalsIgnoreCase("#login")) {
        try {
          client.openConnection();
          System.out.println("Connection established");
        } catch (IOException e) {
          System.out.println("IOException: problem opening client connection");
        }
      } else if(message.equalsIgnoreCase("#gethost")) {
        System.out.println("Host: " + client.getHost());
      } else if(message.equalsIgnoreCase("#getport")) {
        System.out.println("Port: " + client.getPort());
      } else if(message.contains("#sethost")) {
        if(!client.isConnected()) {
          try {
            System.out.println("Set host to: " + message.substring(9));
            client.setHost(message.substring(9));
          } catch(Exception ex) {
            System.out.println("Exception: could not set host");
          }
        } else {
          System.out.println("#sethost only available when client is not connected");
        }
      } else if(message.contains("#setport")) {
        if(!client.isConnected()) {
          try {
            client.setPort(Integer.parseInt(message.substring(9)));
            System.out.println("Set port to: " + message.substring(9));
          } catch (Exception ex) {
            System.out.println("Exception: could not parse port number");
          }
        } else {
          System.out.println("#setport only available when client is not connected");
        }
      } else if(message.equalsIgnoreCase("#help")) {
        //Command list dialog
        System.out.println("SimpleChat ChatClient command list:\n" +
                "#quit \t\t\tuse the client to terminate.\n" +
                "#logoff \t\tcauses the client to disconnect from the server, but not quit.\n" +
                "#sethost <host> calls the setHost method in the client. Only allowed if the client is logged off.\n" +
                "#setport <port> calls the setPort method in the client.\n" +
                "#login \t\t\tcauses the client to connect to the server. Only allowed if the\n" +
                "\t\t\t\tclient is not already connected.\n" +
                "#gethost \t\tdisplays the current host name.\n" +
                "#getport \t\tdisplays the current port number.\n" +
                "#help \t\t\tdisplays command list\n" +
                "----------------------------------------\n");
      } else {
        //display error since no commands were triggered even though '#' symbol was used
        System.out.println("Client input '#' with no command found.\n" +
                "type \"#help\" for command list");
      }
      return true;
    } else {
      return false;
    }
  }
  
  //Class methods ***************************************************

  /**
   * Method gets the port number of client from the console
   * @return inputted port number
   */
  public static String obtainFromConsole(String msg) {
    System.out.print(msg);
    try
    {
      BufferedReader br =
              new BufferedReader(new InputStreamReader(System.in));
      return br.readLine();
    }
    catch (Exception ex)
    {
      System.out.println
              ("Unexpected error while reading from console!");
    }
    return "";
  }

  /**
   * This method is responsible for the creation of the Client UI.
   *
   * @param args The host to connect to.
   */
  public static void main(String[] args) throws Exception {
    String host = "";

    //Obtain the client id from console
    String id = obtainFromConsole("Enter the client id: ");

    if(id.equals("")) {
      //ID mandatory, exit client
      throw new Exception("No client id specified");
    }

    //Obtain the port number from the client
    int port;
    try {
        port = Integer.parseInt(obtainFromConsole("Enter the server port number: "));
    } catch(Exception ex) {
      port = DEFAULT_PORT;
      System.out.println("Server port set to " + DEFAULT_PORT);
    }
    try
    {
      host = args[0];
    }
    catch(ArrayIndexOutOfBoundsException e)
    {
      host = "localhost";
    }

    //Host client on inputted port number
    ClientConsole chat= new ClientConsole(id, host, port);
    chat.client.sendToServer("#login " + id);
    chat.accept();  //Wait for console data
  }
}
//End of ConsoleChat class
