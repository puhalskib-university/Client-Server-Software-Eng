package server;// This file contains material supporting section 3.7 of the textbook:
// "Object Oriented Software Engineering" and is issued under the open-source
// license found at www.lloseng.com 

import com.lloseng.ocsf.server.*;

import java.io.IOException;

/**
 * This class overrides some of the methods in the abstract 
 * superclass in order to give more functionality to the server.
 *
 * @author Dr Timothy C. Lethbridge
 * @author Dr Robert Lagani&egrave;re
 * @author Fran&ccedil;ois B&eacute;langer
 * @author Paul Holden
 * @version July 2000
 */
public class EchoServer extends AbstractServer 
{
  //Class variables *************************************************
  
  /**
   * The default port to listen on.
   */
  final public static int DEFAULT_PORT = 5555;
  
  //Constructors ****************************************************
  
  /**
   * Constructs an instance of the echo server.
   *
   * @param port The port number to connect on.
   */
  public EchoServer(int port) 
  {
    super(port);
  }

  
  //Instance methods ************************************************
  
  /**
   * This method handles any messages received from the client.
   *
   * @param msg The message received from the client.
   * @param client The connection from which the message originated.
   */
  public void handleMessageFromClient
    (Object msg, ConnectionToClient client)
  {
    System.out.println("Message received: " + msg + " from " + client.getInfo("id") + " " + client);

    //make sure to test if msg is a string before checking the substring of it
    if(msg instanceof String) {
      if (((String) msg).startsWith("#login") && client.getInfo("id") == null) {
        //detected login command
        if( ((String) msg).length() > 7 ) {
          // Set id to the client info
          client.setInfo("id", ((String) msg).substring(7)); //client.getInfo("id") = the login id
          System.out.println("Setting " + client + " to have id " + client.getInfo("id"));
        } else {
          System.out.println("error no id");
        }
      } else if(((String) msg).startsWith("#login") && client.getInfo("id") != null) {
        //detect if #login command is used before id is set for the client
        try {
          client.sendToClient("Error \'#login\' detected!");
        } catch (IOException e) {
          e.printStackTrace();
        }
      } else if(client.getInfo("id") == null){
        //#login command not received as the first command
        try {
          client.sendToClient("Error, no id set");
          client.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      } else {
        this.sendToAllClients(client.getInfo("id") + ": " + msg);
      }
    } else {
      this.sendToAllClients(client.getInfo("id") + ": " + msg);
    }
  }
    
  /**
   * This method overrides the one in the superclass.  Called
   * when the server starts listening for connections.
   */
  protected void serverStarted()
  {
    System.out.println
      ("Server listening for connections on port " + getPort());
  }
  
  /**
   * This method overrides the one in the superclass.  Called
   * when the server stops listening for connections.
   */
  protected void serverStopped()
  {
    System.out.println
      ("Server has stopped listening for connections.");
  }

  /**
   * This method overrides superclass method.
   * @param client the connection connected to the client.
   */
  @Override
  protected void clientConnected(ConnectionToClient client) {
    System.out.println("Client connected: "+ client);
    super.clientConnected(client);
  }

  /**
   * This method overrides superclass method.
   * @param client the connection with the client.
   */
  @Override
  protected void clientDisconnected(ConnectionToClient client) {
    System.out.println("Client disconnected");
    super.clientDisconnected(client);
  }

  //Class methods ***************************************************
  
  /**
   * This method is responsible for the creation of 
   * the server instance (there is no UI in this phase).
   *
   * @param args[0] The port number to listen on.  Defaults to 5555 
   *          if no argument is entered.
   */

}
//End of server.EchoServer class
