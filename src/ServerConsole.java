import client.ChatClient;
import common.ChatIF;
import server.EchoServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ServerConsole implements ChatIF
{
    //Class variables *************************************************

    final public static int DEFAULT_PORT = 5555;

    //Instance variables **********************************************

        /**
         * The instance of the client that created this ConsoleChat.
         */
        EchoServer server;


    //Constructors ****************************************************

    /**
         * Constructs an instance of the ServerConsole
         *
         * @param port The port to connect on.
         */
  public ServerConsole(int port) {
      server = new EchoServer(port);
  }
//Private methods
private boolean messageCommand(String message) {
    if(message.contains("#")) {
        if(message.equalsIgnoreCase("#quit")) {
            //terminate connection before exiting program
            try {
                server.close();
            } catch (IOException e) {
                System.out.println("IOException: could not quit server");
            }
        } else if(message.equalsIgnoreCase("#stop")) {
            server.stopListening();
        } else if(message.equalsIgnoreCase("#close")) {
            server.stopListening();
            try {
                server.close();
            } catch (IOException e) {
                System.out.println("IOException: could not close server");
            }
        } else if(message.equalsIgnoreCase("#getport")) {
            System.out.println("Port: " + server.getPort());
        } else if(message.equalsIgnoreCase("#start")) {
            try {
                server.listen();
                System.out.println("Started server");
            } catch (IOException e) {
                System.out.println("IOException: could not start server");
            }
        } else if(message.contains("#setport")) {
            if(server.isClosed()) {
                try {
                    System.out.println("Set host to: " + message.substring(9));
                    server.setPort(Integer.parseInt(message.substring(9)));
                } catch(Exception ex) {
                    System.out.println("Exception: could not set port");
                }
            } else {
                System.out.println("#setport only available when server is closed");
            }
        } else if(message.equalsIgnoreCase("#help")) {
            //Command list dialog
            System.out.println("SimpleChat ServerConsole command list:\n" +
                    "#quit \t\t\tcause the server to terminate gracefully.\n" +
                    "#stop \t\t\tcauses the server to stop listening for new clients.\n" +
                    "#close \t\t\tcauses the server not only to stop listening for new clients, but also\n" +
                    "\t\t\t\tdisconnect all existing clients.\n" +
                    "#setport <port>\tcalls the setPort method in the server. Only allowed\n" +
                    "\t\t\t\tthe server is closed.\n" +
                    "#start \t\t\tcauses the server starts to listening for new clients. Only valid if\n" +
                    "\t\t\t\tthe server is stopped.\n" +
                    "#getport \t\tdisplays the current port number.\n" +
                    "----------------------------------------------\n");
        } else {
            //display error since no commands were triggered even though '#' symbol was used
            System.out.println("Server input '#' with no command found.\n" +
                    "type \"#help\" for command list");
        }
        return true;
    } else {
        return false;
    }
}

  //Instance methods ************************************************
  public static void main(String[] args) {
      int port = 0; //Port to listen on

      try
            {
                port = Integer.parseInt(args[0]); //Get port from command line
            }
            catch(Throwable t)
            {
                port = DEFAULT_PORT; //Set port to 5555
            }

            ServerConsole sv = new ServerConsole(port);

            try
            {
                sv.listen(); //Start listening for connections
            }
            catch (Exception ex)
            {
                System.out.println("ERROR - Could not listen for clients!");
            }
        }

    private void listen() throws IOException {
      server.listen();
        try
        {
            BufferedReader fromConsole =
                    new BufferedReader(new InputStreamReader(System.in));
            String message;
            while (true)
            {
                message = fromConsole.readLine();
                //handle client commands
                if(!messageCommand(message)) {
                    server.sendToAllClients(message);
                    display(message);
                }
            }
        }
        catch (Exception ex)
        {
            System.out.println
                    ("Unexpected error while reading from console!");
        }
    }

    @Override
    public void display(String message) {
        System.out.println("SERVER msg> " + message);
    }
}
//End of ChatClient class
